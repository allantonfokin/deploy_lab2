#!/bin/bash

TARGET="prime_checker"
DEB_PACKAGE="prime-checker"
VERSION="1.0"
INSTALL_DIR="/usr/local/bin"

# Создание папки debian с правилами сборки пакета
mkdir -p debian/"$DEB_PACKAGE"/DEBIAN
echo "Package: $DEB_PACKAGE" > debian/"$DEB_PACKAGE"/DEBIAN/control
echo "Version: $VERSION" >> debian/"$DEB_PACKAGE"/DEBIAN/control
echo "Architecture: amd64" >> debian/"$DEB_PACKAGE"/DEBIAN/control
echo "Maintainer: Anton & Egor & Kirill" >> debian/"$DEB_PACKAGE"/DEBIAN/control
echo "Description: Description of the package" >> debian/"$DEB_PACKAGE"/DEBIAN/control
mkdir -p debian/"$DEB_PACKAGE"/"$INSTALL_DIR"
cp src/"$TARGET" debian/"$DEB_PACKAGE"/"$INSTALL_DIR"

# Сборка deb пакета
dpkg-deb --build debian/"$DEB_PACKAGE"
